# Table of contents

- [Introduction](#introduction)
  * [Background](#background)
  * [Update the handbook](#update-the-handbook)
- [Strategy](#strategy)
  * [Mission](#mission)
  * [Values](#values)
    + [I. Enthusiasm & fun personality](#i-enthusiasm---fun-personality)
    + [II. Time-to-market focus](#ii-time-to-market-focus)
    + [III. App quality](#iii-app-quality)
    + [IV. Strong architecture and clean code](#iv-strong-architecture-and-clean-code)
    + [V. Transparency and openness](#v-transparency-and-openness)
      - [Exceptions](#exceptions)
      - [Openness and honesty](#openness-and-honesty)
      - [Failure](#failure)
      - [Language](#language)
    + [II. Hacker mindset](#ii-hacker-mindset)
    + [III. Constant change](#iii-constant-change)
    + [IV. Ownership](#iv-ownership)
    + [III. Love it, leave it or change it](#iii-love-it--leave-it-or-change-it)
    + [IV. Respect, Friendship and Support](#iv-respect--friendship-and-support)
    + [V. Working remotely](#v-working-remotely)
- [Goals](#goals)
  * [Company 2019](#company-2019)
    + [1. Grow](#1-grow)
    + [2. Delight clients](#2-delight-clients)
    + [3. Become the autority on digital business and remote work](#3-become-the-autority-on-digital-business-and-remote-work)
  * [Teams [WIP]](#teams--wip-)
    + [PM Goals [WIP]](#pm-goals--wip-)
      - [GOAL 1: Improve expectation management and keep client close](#goal-1--improve-expectation-management-and-keep-client-close)
        * [How:](#how-)
        * [Tools for Measurement [WIP]:](#tools-for-measurement--wip--)
      - [GOAL 2: Increase Product Quality](#goal-2--increase-product-quality)
        * [How](#how)
        * [Tools for Measurement [WIP]:](#tools-for-measurement--wip---1)
      - [GOAL 3: Burn down Backlog every sprint](#goal-3--burn-down-backlog-every-sprint)
        * [How:](#how--1)
        * [Tools for Measurement [WIP]:](#tools-for-measurement--wip---2)
- [Goals 2020 [WIP]](#goals-2020--wip-)
- [Responsibilities](#responsibilities)
  * [General](#general)
  * [Permission Management](#permission-management)
    + [Background](#background-1)
    + [Principles](#principles)
- [Roles](#roles)
  * [Team lead](#team-lead)
    + [Philosophy](#philosophy)
    + [Lead by example](#lead-by-example)
    + [Responsibilities](#responsibilities-1)
      - [Improve the company](#improve-the-company)
      - [Recruiting](#recruiting)
      - [Staffing](#staffing)
      - [Technology](#technology)
      - [People development](#people-development)
      - [Training](#training)
      - [Personal support](#personal-support)
  * [Project lead](#project-lead)
    + [Responsibilities](#responsibilities-2)
    + [Code review](#code-review)
  * [Senior developer](#senior-developer)
    + [Responsibilities](#responsibilities-3)
    + [Design and implement new features](#design-and-implement-new-features)
    + [Maintain and manage the existing code base](#maintain-and-manage-the-existing-code-base)
  * [Project managers](#project-managers)
    + [1. Client Communication](#1-client-communication)
    + [2. Jira Hygiene](#2-jira-hygiene)
    + [3. Internal communication](#3-internal-communication)
    + [4. Sprint planning call](#4-sprint-planning-call)
    + [5. Keep documentation and Zeplin up to date](#5-keep-documentation-and-zeplin-up-to-date)
    + [6. Upselling](#6-upselling)
- [Tools](#tools)
  * [Zeplin](#zeplin)
    + [Background](#background-2)
    + [Guidelines](#guidelines)
  * [Slack](#slack)
    + [Background](#background-3)
    + [Profile](#profile)
    + [Channels](#channels)
      - [#general](#-general)
      - [#status](#-status)
      - [#random](#-random)
      - [#programming_X](#-programming-x)
      - [Tackleberry](#tackleberry)
    + [Guidelines](#guidelines-1)
    + [What to use slack for](#what-to-use-slack-for)
    + [What NOT to use slack for](#what-not-to-use-slack-for)
  * [Sketch](#sketch)
    + [Background](#background-4)
    + [Naming convention](#naming-convention)
  * [Hubspot Sales/CRM [WIP]](#hubspot-sales-crm--wip-)
    + [Logging E-Mail conversations](#logging-e-mail-conversations)
  * [Jira](#jira)
    + [Background](#background-5)
    + [Types of Work](#types-of-work)
      - [1. Epic](#1-epic)
      - [2. Tasks](#2-tasks)
      - [3. Sub-Tasks](#3-sub-tasks)
      - [4. Bugs](#4-bugs)
      - [5. Client feedback](#5-client-feedback)
      - [6. Unclear bugs](#6-unclear-bugs)
    + [Versioning](#versioning)
    + [Statuses](#statuses)
      - [Guidelines](#guidelines-2)
  * [GitHub/gitlab](#github-gitlab)
    + [Background](#background-6)
    + [Gitflow](#gitflow)
      - [Perennial branches](#perennial-branches)
    + [Guidelines](#guidelines-3)
    + [Naming](#naming)
    + [Code Review](#code-review)
      - [Mandatory reviewers](#mandatory-reviewers)
  * [E-Mail](#e-mail)
  * [Bitrise](#bitrise)
    + [Background](#background-7)
    + [Guidelines](#guidelines-4)
- [Core processes](#core-processes)
  * [Developer Operations](#developer-operations)
    + [1. Background](#1-background)
    + [2. Staffing](#2-staffing)
    + [3. Timing and Budget](#3-timing-and-budget)
    + [4. The Development Process](#4-the-development-process)
      - [Weekly cycle](#weekly-cycle)
      - [Ticket life-cycle](#ticket-life-cycle)
      - [Guidelines](#guidelines-5)
      - [Pull requests](#pull-requests)
      - [Testing](#testing)
      - [Client feedback](#client-feedback)
    + [Release policy](#release-policy)
      - [Major/Minor Release](#major-minor-release)
      - [Hotfix Release](#hotfix-release)
      - [Major Releases](#major-releases)
      - [Minor Releases](#minor-releases)
      - [Hotfix Releases](#hotfix-releases)
      - [Build numbers](#build-numbers)
  * [Tickets](#tickets)
    + [Ticket live cycle](#ticket-live-cycle)
    + [Project setup Android](#project-setup-android)
    + [Release Procedure Web Projects](#release-procedure-web-projects)
      - [1. During Development](#1-during-development)
      - [2. Creating a testable version](#2-creating-a-testable-version)
      - [3. Go Live](#3-go-live)
  * [Documentation](#documentation)
    + [Template](#template)
      - [1. Jira Epic](#1-jira-epic)
      - [2. User stories](#2-user-stories)
      - [3. Requirements](#3-requirements)
      - [4. Architecture](#4-architecture)
      - [5. Views](#5-views)
      - [6. Tracking](#6-tracking)
      - [7. Links](#7-links)
      - [8. Todos](#8-todos)
  * [API Guidelines](#api-guidelines)
    + [Background](#background-8)
    + [Preferred solution: 100% control](#preferred-solution--100--control)
      - [1. Own API](#1-own-api)
      - [2. Firebase](#2-firebase)
    + [Third-party APIs](#third-party-apis)
    + [Monitoring](#monitoring)
  * [CDCI](#cdci)
    + [Web](#web)
    + [Web Gitlab](#web-gitlab)
    + [Gitlab CDCI Web Guidelines](#gitlab-cdci-web-guidelines)
    + [Steps](#steps)
      - [0. Get the project deploy from your local terminal](#0-get-the-project-deploy-from-your-local-terminal)
      - [1. Set up variables in settings/cd_ci](#1-set-up-variables-in-settings-cd-ci)
      - [2. Set up CDCD file in root](#2-set-up-cdcd-file-in-root)
      - [3. Todos](#3-todos)
- [People operations](#people-operations)
  * [Recruiting](#recruiting-1)
    + [Guidelines](#guidelines-6)
    + [Process](#process)
  * [Contracts](#contracts)
    + [Freelance vs. Full time employee](#freelance-vs-full-time-employee)
  * [Compensation](#compensation)
    + [Background](#background-9)
    + [Developers and project managers](#developers-and-project-managers)
    + [*QA manager*](#-qa-manager-)
    + [Full-time employees](#full-time-employees)
  * [Onboarding](#onboarding)
    + [1. E-mail](#1-e-mail)
    + [2. Slack](#2-slack)
    + [3. Github and Gitlab](#3-github-and-gitlab)
    + [4. Toggl](#4-toggl)
    + [5. Jira](#5-jira)
    + [6. 1password](#6-1password)
    + [7. Zeplin](#7-zeplin)
    + [8. Invision](#8-invision)
    + [9. Holiday Calendar](#9-holiday-calendar)
    + [10. Applause](#10-applause)
    + [13. Add birthday and anniversary to Hannes Calendar and Spreadsheet](#13-add-birthday-and-anniversary-to-hannes-calendar-and-spreadsheet)
    + [14. MyTaxi](#14-mytaxi)
    + [15. Trello](#15-trello)
    + [16. Zohoo (Employees only)](#16-zohoo--employees-only-)
    + [16. TODO: Setup Jira/gitlab + Toggle Sync:](#16-todo--setup-jira-gitlab---toggle-sync-)
    + [17. After they accepted everything:](#17-after-they-accepted-everything-)
  * [Continuous Performance Management](#continuous-performance-management)
    + [1 on 1 reviews](#1-on-1-reviews)
  * [People development](#people-development-1)
    + [Background](#background-10)
    + [0. Performance review](#0-performance-review)
    + [1. Goals](#1-goals)
    + [2. KPI](#2-kpi)
      - [Developer KPI](#developer-kpi)
      - [QA KPI](#qa-kpi)
      - [Project management KPI](#project-management-kpi)
    + [3. 360° Feedback](#3-360--feedback)
      - [I. HOW TO](#i-how-to)
      - [II. EXAMPLES](#ii-examples)
      - [III. NOTES](#iii-notes)
    + [4. Leadership feedback](#4-leadership-feedback)
  * [Training](#training-1)
    + [Background](#background-11)
    + [Courses](#courses)
    + [Masterclasses](#masterclasses)
    + [Book club](#book-club)
  * [Meet-ups](#meet-ups)
      - [Retreats](#retreats)
      - [Workshops](#workshops)
      - [Grill the boss](#grill-the-boss)
      - [Meet a Woody](#meet-a-woody)
  * [Travel and Expensing](#travel-and-expensing)
    + [Background](#background-12)
    + [Booking process](#booking-process)
    + [Hotels](#hotels)
    + [Flights](#flights)
    + [Dinners](#dinners)
    + [What we pay for](#what-we-pay-for)
    + [Expensing](#expensing)
      - [Freelancers](#freelancers)
      - [Employees](#employees)
  * [Time Tracking](#time-tracking)
    + [Background](#background-13)
    + [Working hours](#working-hours)
    + [Public holidays and vacations](#public-holidays-and-vacations)
    + [Workshops](#workshops-1)
    + [Travel](#travel)
    + [Masterclasses](#masterclasses-1)
    + [Sales](#sales)
    + [Naming](#naming-1)
  * [Offboarding](#offboarding)
    + [Process](#process-1)
    + [Last day](#last-day)
      - [Ops](#ops)
      - [MD](#md)
      - [Person leaving](#person-leaving)
  * [Alumni](#alumni)
- [Support processes](#support-processes)
  * [Accounting](#accounting)
    + [Tools & Processes](#tools---processes)
    + [Tags](#tags)
    + [Cost centres](#cost-centres)
  * [Freelancer Invoices](#freelancer-invoices)
  * [Processing invoices](#processing-invoices)
  * [Sales and Marketing](#sales-and-marketing)
    + [Our skills](#our-skills)
      - [Digital Transformation](#digital-transformation)
      - [Product management](#product-management)
      - [Distributed](#distributed)
    + [Passions](#passions)
    + [Tilt](#tilt)
    + [Segments](#segments)
    + [Channels](#channels-1)
    + [Content](#content)
      - [1. Distributed](#1-distributed)
      - [2. Product management](#2-product-management)
      - [3. Digital Transformation](#3-digital-transformation)
    + [Marketing Operations](#marketing-operations)
      - [1. Social Media](#1-social-media)
      - [2. Blog](#2-blog)

# Introduction

## Background

We are to be "transparent by default". We want to be equally transparent to potential clients and our team both current and future. As such this document is open-source.

We also want to be an active part of the remote-work community and want to share what we learned with other startups to bring the wonders of distributed to the world.

## Update the handbook

Feel free to ask questions through [issues](https://gitlab.com/stanwood/handbook/issues/new) and contribute by creating [merge requests](https://gitlab.com/stanwood/handbook/merge_requests/new). When creating an issue, use a link to the line you are referring to for easy access, i.e.
https://gitlab.com/stanwood/handbook/blob/master/mission.md#L5.

Make sure to update the TOC after you added or edited headlines. https://ecotrust-canada.github.io/markdown-toc/

# Strategy

## Mission

stanwood exists to build excellent technology in a fun and relaxed way.

We want to build the largest distributed digital agency in the world to bring the joys of remote work to more developers.

To achieve that, we need happy clients.

Clients are happy when we deliver excellent products in time and budget and give the clients a sense that we are on top of things at all times.

## Professional conduct

### I. Enthusiasm & fun personality

We are fun to work with (wink)

We are outgoing, confident, charming, helpful and honest.

We communicate openly and timely.

We are not shy, hide in slack or keep grudges.

### II. Time-to-market focus

We focus on shipping stuff to the client and his/her users.

We are super efficient.

We prefer building things over having meetings.

### III. App quality

We produce fantastic user experiences.

- Beautiful UI
- Easy to use
- Rock stable
- Super fast

### IV. Strong architecture and clean code

We standardise our architecture to enable fast project switching.

We actively propose changes to our standard architecture.

We write clean code. That includes

- Separation of Concerns
- Dependency Injection
- Single Responsibility Principle (but not strictly adhered to)
- Testable Code
- Short functions - should be visible on the screen
- Clear and understandable naming

## Values

### I. Transparency and openness

We believe that transparency in itself is a virtue. The more we act in the open, the less likely we are to fall victim to internal politics or mistrust.

We also want to enable potential clients and team members to know, what they are facing.

#### Exceptions

**Very private **

Some documents are not made public reside in the GDrive of the MD.

1. All data considered private under data protection regulations or other laws.
2. Contracts with freelancers and employees.

**Internal**

Some documents are only available to team members and not the public. They reside in databases, GDrive, Debitoor or Github.

1. Production client data - only accessible by admins.
2. Client contracts and offers.
3. Slack conversations - except direct messages or confidential discussions in #leads_private.
4. Code and tickets on non-open-source projects
5. Passwords and production keys (1password).

#### Openness and honesty

We prefer a hard truth over mollycoddling. However, you can tell even a hard truth kindly.

We embrace numbers and data over gut feeling or single experiences.

However, we also know, that data is only a small part of the truth.

We do not misuse numbers. We know, they are biased and need interpretation.

#### Failure

Human beings have been wrong most of the time in their history.

We try to fail often and fast.

Failure is the best way to learn.

We admit failures - even brag about them.

#### Language

stanwood is a (UK) English speaking company.

Except for a very few English is not our first language and quite frankly, it could use a bit of work.

All written and oral communication must be in English. Yes, even between to countrymen.

We try to avoid abbreviations and industry speak as much as possible to enable anyone reading our stuff to follow up.
Only use jargon, if there is no equivalent in ordinary English.
That also helps not coming across pompous ;-)

We use UK English both written and spoken.

We refer to employees and freelancers as "team members" or "Woodies". New Woodies are called "Wookies".

When talking about people we use the plural to avoid female or male pronouns, we try to use neutral terms where-ever possible. If not possible we alternate between female and male pronouns.

### II. Hacker mindset

We work smart:

Automate everything.

Reuse existing solutions.

Do not reinvent the wheel.

We work super efficient.

We understand the 80/20-rule.

We continuously review our tools and processes.

We believe in teamwork.

Results and time to market is far more valuable than personal credit.

### III. Constant change

We always try to improve ourselves and others.

We always try to improve our processes and policies: They should help, not hamper.

We are open to new suggestions and improvements.

We try out new things as soon as possible.

### IV. Ownership

We take personal responsibility for our products.

If you find something broken, we make sure you fix it.

If someone must do something, we pound them into action - repeatedly.

We take personal responsibility of tasks.

We never ignore an email or post when someone uses our handle.

We acknowledge questions or tasks within 1h in slack or 24h in an email.

If we cannot complete it right away, we give an ETA.

Should we not be able to hold that ETA, we inform the other with a new ETA asap.

We test and iterate our output and our processes as often as possible.

### III. Love it, leave it or change it

We are hackers - we do own things the best we can despite imperfections of the outside world.

Instead of complaining, we focus on what we can control.

### IV. Respect, Friendship and Support

We are a family and talk friendly, respectfully and helpful.

The team is more important than the individual.

Unless someone falls on hard times: sickness, personal problems, financial issues.

In those cases, we support unconditionally.

We embrace diversity: Nationality, language, religion, gender, sexual orientation, disabilities or mental health.
We cannot have it colourful enough.

We don't disclose what someone tells us in trust.

We support each other wherever we can.

We don't let people go lightly.
We review, feedback, mentor and give second chances.
We help people we let go, to find a new job.

Sexual harassment or discrimination, both active and by passive toleration is not.

We do not tolerate sexual harassment, discrimination based on

- Age
- Disability
- Race
- Nationality
- Religious belief or lack of religious belief
- Life expectancy
- Sex
- Sexual orientation
- Transgender status

We do not tolerate violent acts, threats or encouragement of violence, fighting, bullying, coercion or use of abusive or threatening words directed to, about or against a team member, client or third party, even in a joking manner. First-time violations of this value is a fireable offence.

We kindly nudge each other should we, for instance, use the male form of a word by adding the eggplant emoji to the post.

### V. Working remotely

We work where we can work best: near our families, at those places we choose to because we want, not because we must.

We treat each other the same - despite our location.

Working remotely can be a challenge:
When someone annoys us, we assume ignorance before bad intentions.

Make an extra effort at communication.

We want to be able whenever where-ever and how much we want. Towards that end, we build our processes around asynchronous work, document everything and try to avoid the need for real-time communication and collaboration.

We do not discriminate for your choice of location of our team members. We pay the same whether you live in London or Bangkok. However, that also means we do not pay for travel time.

# Goals

## Company 2019

### 1. Grow

- Funnel (hours offered) +400% (6 m€) @sales
- Revenue +100% (4 m€) @sales
- Profitability 30% EBIT margin @dev_teams @pms

### 2. Delight clients

- Customer satisfaction 90% @pms
- Sprint goals hit 90% @dev_teams
- Major release budgets hit 90% @pms
- Production and client bugs < 5% of total tickets @qa

### 3. Become the autority on digital business and remote work

- 200 mini articles @karinkleist
- 1000 social followers @karinkleist
- 12 presentations at conventions @hanneskl
- Meet 10 remote MDs @hanneskl

## Teams [WIP]

### PM Goals [WIP]

#### GOAL 1: Improve expectation management and keep client close
##### How:
1. At least once a week: update the client about: *budget, blocker, progress*.
1. Ask the clients how updated they feel (one NPS question/month, one subjective PM estimation per week)
1. underpromise and overdeliver

##### Tools for Measurement [WIP]:
1. All client communication goes through hubspot. If a client question gets not answered in 1 day, we will get an alert. (operationalization still to be discussed)
2. For all active clients the PM gets a form from hubspot once a week to answer:
1. Did you update the client about: budget, about blocker, about progress?
1. How do you rate the client satisfaction? Customer Satisfaction Score (CSAT) 1-10

Answers get recorded in the company property.
1. Once a month, the client gets a short Mail with asking for *Net promoter score* survey.

#### GOAL 2: Increase Product Quality
##### How
1. session based crash-free rate above 99%
2. 0% critical bug-tickets reported from the client for tickets with status "done", < 5% other bugs @qa
3. 90% of tickets pass QA at the first attempt @dev_teams
4. average app rating in stores is >4. Measured by % of 5 stars in that year.

##### Tools for Measurement [WIP]:
1. firebase session crash rate
2. client mails with bug reports, ticket created with a label (PM sets label) to be recognizable as client reportet
3. qa ticket rejection rate
4. gathered by App Annie rating

#### GOAL 3: Burn down Backlog every sprint

##### How:
1. 100% burndown of all sprint tickets
2. Average Time a ticket is in QA stays below 24h (during working days)
3. We send a QA checklist with all we have QA'd to the client at Friday with the build

##### Tools for Measurement [WIP]:
1. milestone is with 100% at end of sprint
2. report from gitlab
3. to be checked if reported in client communication (hubspot conversation)

# Goals 2020 [WIP]

- Become more async


# Responsibilities
## General

We need to have clear policies on

- who can decide things
- who needs to be asked for advice
- who should propose

Of course, we always strive to involve everyone in all discussions.

| Topic | MD | Team Leads | Heads | PMs | Developers | Examples
| - | - | - | - | - | - | -
| Initial project staffing | Advise | Decide |  | Advice |  |
| Ongoing project staffing | Decide | Advise |  | Advise |  |
| Coding practices |  | Decide |  |  | Advise | Switching to a new language, dependency management tool, 3rd party libs, coding style
| Stack | Decide | Advice | Advice |  |  | Native code vs reacting/xamarin/vue/angular, Google App Engine, AWS, Python vs Node.js, Firebase vs Azure
| Tools | Decide |  | Advice |  |  | Github vs Confluence, Slack vs Hipchat, CD/CI Tools
| Internal frameworks | Decide | Advice |  |  | Propose | Analytics, Rating, Core...
| Recruiting | Advise | Decide |  |  | Propose | Finding and hiring new devs
| Training | Advise | Decide |  |  | Advise | Masterclasses, pair programming, Udemy courses
| Personal development | Advise | Decide |  |  |  | Leadership training, language classes, mentoring
| Contract extension | Advise | Decide |  |  |  | Freelancers (1x per year), full-time employees after a trial period
| Promotions | Advice | Decide |  |  |  | Mid > Senior > Project lead

## Permission Management

### Background

We are facing four opposing principles when it comes to permissions.

1. Transparency and openness by default: Every team member should be able to read and change anything.
2. CD/CI: We need to save guard against breaking our continuous integration and deployment or production environments.
3. Data protection: Private end-user data (databases, logs etc.) should be visible only to the smallest amount of people.
4. We do not want a bottleneck on permissions should an admin be not available.

### Principles

1. Data protection: Production data needs to be limited to the least amount of people possible and reviewed regularly.
2. Code and documentation: Every team member has read permissions.
3. Our Head of Automation closely guards writing-permissions.
4. Contact the Head of Automation in #operations in Slack if you need more permissions.
5. Every admin permission should be with three people: MD, Head of backend development, Head of Automation.



# Roles
## Team lead

### Philosophy

Leadership means: "Take the blame, share the fame."

1. As a leader, everything is always your fault. So you should still take actions to fix situations.

Examples:

- if one of your team members is unhappy or not performing
- if a client or project manager is unresponsive, unclear in his briefings or confused
- if a project is not running well
- if the MD is a tyrant

2. As a leader, you are only a facilitator. Your team does the actual work and deserves all praise.

### Lead by example

- Demonstrate our values, policies and processes every day
- Push back on poorly described tickets and complex solutions

### Responsibilities

#### Improve the company

- Proactively identify processes and policies that need improvement and push for change
- Help the other people and leads to improve their team's procedures and policies
- Analyse data to find patterns in our work

#### Recruiting

- Define developer recruitments standards
- Interview and recruit new developers
- Push the applicants through the process

#### Staffing

- Coordinate project roster
- Make sure the team is well mixed and get the chance to work with everyone.
- Navigate and support project leads to ongoing projects.

#### Technology

- Push the boundaries of your platform OS capabilities
- Find a team member per framework and make the evangelists
- Define and improve the company's system architecture
- Liaison with the other platforms to align practices

#### People development

- Oversee team development, and policy implementation
- Propose promotions and developers who need review
- Prepare and establish quarterly team review

#### Training

- Plan, organise and manage 3 annual workshops
- Develop individualised training per team member
- Work alongside the PM's to utilise the best working environment

#### Personal support

- Support the team with any issues they have, and identify strength and weakness
- Develop career plans for every team member

## Project lead

Project leads oversee the project development, are the direct contact for PM's and manages the team to meet best coding practice, release dates, and budget.

### Responsibilities

- Be the single contact for PM's
- Review ticket description
- Enforce stanwood architecture
- Estimate new tickets
- Delegate the work to their supporting team
- Code review
- Single contact for PM's
- During the ongoing project development, you are the primary contact for PM's:

- Keep them up to date with current feature development
- Inform of over budget tickets
- Discuss release dates
- Communication is vital for the success of the project.

- Review ticket description
- Create subtasks if needed
- Reject features due to the complexity
- Reject tickets that do not include all the information needed for implementation
- Missing endpoints
- Missing design
- Bug tickets: missing Crashlytics link, steps to replicate, device information
- Missing full description of requirements
- Enforce stanwood architecture
- Making sure to enforce the architecture on all projects.

- Estimate new tickets
- Break down tickets to subtasks if needed.

- Delegate the work to their supporting team

As part of being a project lead, you manage up to three active projects. You spend 30% of your time and delegate most of the tickets to the supporting developer.

### Code review
Making sure code review is done by stanwood best practice and follow the guidelines. PR/CR Guidelines

## Senior developer

As Senior developers, you have significant experience in software development, knowledge of industry practices and you are the backbone of software development at stanwood.

### Responsibilities

- Liaise with project leads
- Implement stanwood architecture
- Design and implement new features
- Push back on features that add complexity
- Ongoing bug fixes, and maintenance
- Keep project documentation up to date
- Maintain and manage the existing code base
- Liaise with project leads on new deadlines and new features

### Design and implement new features
As a senior developer, you do most of the heavy lifting during development.

Push back on features that add complexity
Push back on features that add complexity to the project lead. We are not here to solve every client feature request but look for the quickest, and safest solution that supports the code base.

Ongoing bug fixes, and maintenance

Keep project documentation up to date
Work alongside the project lead and update the documentation on a regular basis.

### Maintain and manage the existing code base
Make sure we are not adding technical debt when maintaining the code base. Should there be a case where the current code base does not support ongoing development, involve the project-lead?

## Project managers

Order of priorities:

### 1. Client Communication

- Keep client informed about progress and blockers 3x per week.
- Send one personal mail
- React within 15 minutes to calls.*
- React within 4 hours to emails*
- Create tickets to all issues raised within one day.*

* During business hours.

### 2. Jira Hygiene

Once a day: 

- zero the inbox
- ping project leads to estimate - if ticket update > 24h
- ping assignee - if ticket update > 1w
- Ensure:
- tickets are well described
- documentation regarding that ticket is up to date
- everything discussed in Slack is synced back to Jira

### 3. Internal communication

1. Keep your lead and all devs up to date of milestones, next milestones, of the pressing issues, blockers and goals 3x per week.
2. Send an update post for all your projects in slack channel #done friday until 12:00. Keep the post numbered and think about it as a logbook.
3. for every project, include status, blockers, next milestones
4. is there a sprint in the following week, also include when the *sprint planning call* has been and who attended the planning call(s) (client, dev-teams, qa)

### 4. Sprint planning call

At least every Thursday before the sprint: Grab the client and your team (including QA) and fill the backlog of the team according to the sprint planing spreadsheet. 
Make sure all tickets are well described before the sprint starts.
All Devs should be able to work on it without further questions.
Include acceptance criteria for QA.

### 5. Keep documentation and Zeplin up to date

### 6. Upselling

Suggest features and improvements to clients.


# Tools
## Zeplin

### Background

We use Zeplin to share and discuss designs and also to host design assets and documentation for the developers.

### Guidelines

- Designers: Upload files there and puts sketch file on GDrive
- Tickets live in Jira. However, only for briefing and status.
- Commenting happens in Zeplin
    - Yellow: Todo
    - Orange: Discussion
    - Green: Documentation
- Upload a photo and a profile picture to Zeplin.
- Use the same handle as in slack.

## Slack

### Background

We use slack as our daily real-time communication tool for projects and our central non-project communication.

### Profile

Add a full name, picture, phone number and email.

Username: @firstname if taken @nickname

Same on every other platform that we use.

### Channels

#### #general
Official stuff. Only the MD posts here.

#### #status

Post "Good morning", "be-right-back", "back", "good night" + what are you going to do today.

The first line of your post and the first emoji becomes your profile status message. Use it to communicate your availability to people who try to reach you.

When leaving for vacation, please use this template:
`:desert_island: on vacation till August 23rd.`

#### #random

GIFs, cat videos etc. to

#### #programming_X

General programming talks and ask for help

#### Tackleberry

Our very own police bot are be sent you reminders that require your actions from time to time.

- Tickets that need your estimation
- Tickets that need to be moved
- Tickets that have are already 50% over estimate: Update the `Remaining estimate` and inform your pm
- Tickets that have are already 100% over estimate: Update the `Remaining estimate` and inform your pm

### Guidelines

When responding to todos in slack, open a thread, use "On it {time}" (i.e. "On it in 1h") and "Done"/:white_check_mark:

Project channels starting with "_". All real-time discussion belongs there. acknowledge

Jira, Zeplin, Github, Bitrise post to the channel project channel.

Please do not hold project talk in direct messages. We lose that information to the company. Use a public channel instead.

When asking someone something, always use @username. Some developers are a member of dozens of channels and might miss your question.

Always use threats.

### What to use slack for

- Posting FYI information
- Asking questions
- Assigning non-project todos
- Fun and giggles
- Short discussions (max five posts)

### What NOT to use slack for

- Real-time discussions: Jump on a call.
- Async discussions: Use Jira
- Async questions or todos (like asking someone who is on vacation more than 1 question per channel)

## Sketch

### Background

We use Sketch for designing all our apps and websites.

### Naming convention

- Digit 1: Epic
- Digit 2: View
- Digit 3: Variant

Example: 1.1.2 Onboarding - Welcome Screen - Disabled

## Hubspot Sales/CRM [WIP]

### Logging E-Mail conversations

*DO NOT* connect your Gmail to hubspot

1. Make sure you have hubspot sales access (free is ok)
2. Add your sending addresses to your email aliases under `Profile & Preferences` in Hubspot
3. Install the [hubspot sales extension](https://chrome.google.com/webstore/detail/hubspot-sales/oiiaigjnkhngdbnoookogelabohpglmd?hl=en&hsCtaTracking=650e1a05-dcfb-4d71-905f-c45130741113%7Ca5a89f8f-84cf-4783-bfac-0fc86cdc4789&__hssc=20629287.2.1546784988350&__hstc=20629287.18bf70c1f9f3c6ad0c95a8381712ec66.1528271662874.1546781634695.1546784988350.33&__hsfp=1062686129). 
4. *Sending:*
Activate `Log` and `Track` in the compose-view when **sending** Mails. If it's a known contact with GDPR consent, Hubspot will add the log to the contact.
If have no Chrome, use `4628060@bcc.hubspot.com`
5. *Receiving client communication:*
If the client did not cc is to our sales address, the mail will not get logged.
Forward the mail **before** replying to `4628060@forward.hubspot.com`
6. If a client does not accept our GDPR terms, add him/her to `Never log` in hubspot [CRM settings](https://app.hubspot.com/sales-products-settings/4628060/bcc)

## Jira

### Background
We use Jira as our central tool for organising our development process around tickets.

### Types of Work

#### 1. Epic

- Group of tasks for a section in the app or topic.
- Usually along the user flow in the app:
- Design, Setup, Onboarding, Section 1, Section 2..., SDKs, Release
- We try to finish epics in a development cycle so that the client and our testers can thoroughly review that block.
- Tickets that the client actively decides not to implement or fix are assigned to the Version Later to hide them out of the backlog to keep the backlog relevant and up to date.
- Each Epic has a link to the GitHub Documentation of that Epic.
- Each Epic needs to have these elements:
        - Link to Zeplin designs
        - List of all UI elements
            - Where is the element getting its data?
            - Where is the aspect of writing it's data to
        -A list a and description of all interactions including view did appear
        - data was loaded successfully
        - an error occurred when loading the data
    - all possible user interactions

#### 2. Tasks

Tasks have a link to the GitHub documentation and Zeplin of that Task.

#### 3. Sub-Tasks

Tasks are broken down by the project lead into Sub-Tasks.

Each task is assigned to one platform and one developer.
Tasks should not be more than 4 hours or split up into subtasks
Tasks should be linked to corresponding functions on other platforms.

MUST NOT have any requirements. Those belong into the Task
Estimation and logging of hours must happen on sub-tasks

#### 4. Bugs

Clients and testers create bug tickets. Bugs must contain:

- Steps to reproduce
- Screenshot (ideally from TestFairy)
- Details on the device, OS, user and state

#### 5. Client feedback

Clients and Crashlytics create issues as "Client Feedback". The project manager details and reproduces those before changing the type to "Bug".

#### 6. Unclear bugs
If we cannot reproduce a bug, we keep it in the Backlog as an "Unclear bug" assign the epic Later in the hope that someone can reproduce the bug.

### Versioning

We use versions as an additional tool to organise tickets. The rules are loose.

However: When releasing a version to testers and clients, a version must put together with all implemented tickets since the last release.

### Statuses

[X. Statusname [Person in charge]: Description]

Inbox [PM]: Clients and tools dump stuff in here, project managers add details

Needs Estimation [Project Lead]: Developer adds an "Original Estimate". // Suggestion: [(developer_estimation * 1.5 )+ 15%(for Code Review)]

Needs Approval [Client or PM]: Clients approves/declines.

Approved Backlog [Project Lead]: Approved by the client, but not ready to be taken by a developer in this sprint

Ready for Development [Dev]: Sprint backlog, filled by the project manager. A developer takes the ticket from there and moves to next status.

Working On [Dev]: Developer implements, (if UI-task: adds screenshots), assigns other dev to code review

Code Review [Dev]: reviewer approves, assigns author, author merges to dev, the author moves to QA with QA-instructions

QA [PM or QA Manager]: Project managers: reviews or asks QA-Manager to review. if failed: Assigns back to the author working on if approved: moves to client review (if no client: done)

Needs client review [Client]: Client approves/declines 

Done [PM] (Won't fix [PM]: Tickets, that are not solved and need no further attention) (Duplicate [PM]: Duplicates, need to be linked to the original first) Crashlytics Issues

The PM should create a bug ticket and add the issues link in the description.

The developer should close the issue on Crashlytics once he fixes a bug and the ticket moves to Prio: 'Blocker' We use this to indicate that we cannot proceed with a ticket due to something missing by someone else. i.e. an API to be finished or a design to be created.

The project manager reviews those blockers regularly to check if the constraint has been lifted.

#### Guidelines

Discussions on issues belong in the comments in the issue. If we discuss something is discussed in slack or a call, the project lead is responsible for summarising the decision on the ticket.

## GitHub/gitlab

### Background

We use Github/gitlab to host our code.

### Gitlab Structure

Although gitlab is currently only under consideration we use the following structure of groups

/handbook (the handbook)

/internal (internal documents)

/open-source/`{project key}` (open source proejcts)

/apps/`{client key}`/`{project key}` (client projects)

### Gitflow

We are using a custom Gitflow.

#### Perennial branches

- master: deployed versions
- develop: clean development version, for testing and beta release
- stage: merged from `develop` for testing (not on Android)

### Guidelines

- Never push or merge directly into master and develop. Only reviewed pull requests are allowed to be merged.
- All feature/hotfix branches must be merged into the release branch via pull requests during the sprint
- Commit & push approx. all 3 hours
- Create pull request for every issue before you move to "Code Review" and assign the reviewer in GitHub
- On "go live": Project lead merges to master, creates a tag and adds release notes from Jira

### Naming

For Jira and Toggl to auto import the code changes, we need to follow some naming conventions.

- Pull request: 
	- _github_: SEL-123 {Ticket description}
	- _gitlab_: #123 Ticket description 
		- reference the ticket in the description as well, e.g. "See #123" to have an easy to use backlink to the original ticket so that the reviewers can check the ticket contents and can enable time tracking on that ticket. 
		- **Remove the default "Closes #123" as it will close the ticket right after merging which is NOT what we want**
- Branch: 
	- Use `-` for whitespaces in the branch name (just as gitlab does when it auto-creates the branch for you), don't use `_`
	- _github_: feature,hotfix,bugfix/SEL-123-{ticket-description}, e.g.
		- feature/SEL-12-awesome-feature
		- hotfix/SEL-13-awesome-hotfix (fixing something in master)
		- bugfix/SEL-14-awesome-bugfix (fixing)
	- _gitlab_: 123-{ticket-description} (best create the branch directly from the ticket via the web UI and then check it out locally)
- Commit: 
	- _github_: SEL-123 {commit description}
	- _gitlab_: #123 {commit description}

`ticket-description` in branch names does not have to be the same as `Summary` in Jira/gitlab as there can be multiple branches per ticket. You can rephrase it as long as the meaning does not get lost.

### Code Review

PR/CR Guidelines

Only use GitHub comments for the initial and short discussions. More extended discussions should happen in Slack (public project channel). Report the results (+link) of that discussion in GitHub.

When we add new libraries which have not been pre-approved via the library list, please include one of the following developers as a reviewer and describe why you used that particular library (and what possible pitfalls it has) in the PR description.

#### Mandatory reviewers
iOS: Tal

Android: the project lead

Web: Arni

## E-Mail

E-Mails are slow (having to add all those "Hi there, kind regards") and get
un-readable when more than 2 people start responding in a threat.

Also, you cannot leave a threat.

1. E-Mails are not allowed for internal communication - except during vacation and to forward attachments like invoices.

2. When forwarding documents, never zip them. Always use

3. Learn about [filters](https://support.google.com/mail/answer/6579?hl=en)
and [search](https://support.google.com/mail/answer/7190?hl=en) suffixes in Gmail.

## Bitrise

### Background

Bitrise is our CD/CI for Android and iOS apps.

### Guidelines

- `master` and `develop` must always build
- `master` deploys to App Stores or production web server
- `develop` deploys to clients Monday 9 am



# Core processes
## Developer Operations

### 1. Background

We love to develop apps and websites which will be loved by users and clients.

We want to be transparent and dependable for our client. We also want to sync crowd testing and processes of our clients as well as reduce miss-understandings and unclear tickets.

At last, we want to limit crossfire from clients throughout the week and "jumping" in and out of projects.

### 2. Staffing

We staff every project.

- With a project manager
- One platform lead and one (or more) supporting frontend developer per platform
- One (or more) backend developers

### 3. Timing and Budget

We practice Rapid Software Development. We aim to get apps to the store within 8 weeks after the briefing.

-  week 1: concept, wireframing and architecture
- week 2: design, estimation, setup
- weeks 3-6: coding (3 development cycles)
- weels 7-8: testing and fixing (by crowd and client)

Overhead: We should not spend more than

- 15% on project management (including the project manager!)
- 15% on fixing bugs after releasing the first beta
- 15% on QA and testing
- 15% on refactoring

### 4. The Development Process

#### Weekly cycle

- Project manager moves the weeks worth of tickets to `Ready for Development` before Friday noon.
- Weekly staffing and update meetings on Friday between project managers and team leads
    - What did we achieve/not finish last week?
    - Which epics are we tackling this week?
    - Roadblocks: We raise them. However, do not solve them.
    - Solving staffing issues and bottlenecks
- After: Team and project leads go through `Ready for Development` and delegate tickets to the team
- Development
- Developers: Implement and review all tickets from the development cycle ready for development till Thursday evening.
- Project manager/QA managers: Reviews all tickets on develop branch and report bugs.
- Developers: Fix those bugs.
- Project leads to ensure that `develop` is working and testable build deployed to TestFairy
- QA manager reviews the feedback from the testing agency and creates bugs tickets in Jira.
- Project manager distributes the test version to the client with release notes for review

#### Ticket life-cycle

- Project manager creates a ticket, adds the relevant description and moved to `Needs estimation` and assigned to Project lead
- Project lead reviews the ticket for sufficient information and estimates 1h/ 2h/ 4h
- Once estimated, the ticket is moved to `Needs approval` and assigned to project manager
- Project manager (or client) approves, moves the ticket to `Approved backlog` and assigns the project lead
- On Friday, planned tickets for the following sprint are moved to `Ready for development`.
- The developer grabs an assigned ticket in order of priority and moves to `Working on`.
- Once the ticket is complete, the developer creates a Pull Request assigned to the project lead and moves the ticket to `Code Review.`
- Project lead approves the PR and assigns the ticket back to the developer
- The developer moves the ticket to `QA` when the branch is merged to develop and assigns the QA manager to test the ticket
- The QA approves the ticket and moves to Done; the original developer is assigned again
- The QA manager moves rejected tickets are moved to `Ready for development.`

#### Guidelines

The person responsible for the next step must be assigned to the ticket. i.e. when asking a question that question that blocks the ticket, please assign the person you asked.

Order of work: Weekly priority sent up until Monday; Priority: Critical, Status: Needs estimation, QA, Code Review, Working on, Ready for Development

All tickets must receive

- Code review
- App/web: a functional review by the Project manager or QA manager
- API: a functional review by front-end developer

#### Pull requests

- Developers create a pull request from their feature/hotfix/bugfix branch to develop branch.
- Assigns the Project leads for code review on GitHub.
- Project leads reviews code and approves PR (reviewer is not merging PR)
- Author of pull request merges to develop and moves the ticket to QA.

#### Testing

The project lead developer creates a testable version on Friday EOD.

A QA manager ships a testable build to the testing agency.
Each release is tested by internal testers or our test agency over the weekend.

#### Client feedback

The client gives feedback throughout the week (report bugs, adds request changes).
For that, they are invited to do that directly in Jira.

Only tickets we get till Thursday can be part of the next development cycle.

Project managers review, reproduce and detail client requests/bug reports and creates and details Epics, breaks down to Tasks and coordinates priorities with the client on an ongoing basis.

The project lead breaks down Tasks to sub-tasks for each platform and create API sub-task tickets, estimates sub-tasks and tasks (if no subtasks) and moves them to `Needs approval`.

### Release policy

#### Major/Minor Release

Monday-Wednesday. Thursday if critical.

#### Hotfix Release

Hotfixes on a Friday, only if 10%+ users are affected.

#### Major Releases

Are used sparingly — only 1-2 major releases per year.

1.0, 2.0, 3.0

#### Minor Releases

Every release with features in it increments the second digit of the version.

1.1, 2.3, 2.5

#### Hotfix Releases

Releases that merely fix bugs are incremented with the third digit of the version.

1.1.1, 2.3.4

#### Build numbers

We use automatically increasing build numbers from Bitrise. We use the same build version as Bitrise build.

## Tickets

### Ticket live cycle

|     Topic     |     Project manager     |     Project Lead     |     Developer
|     -     |     -     |     -     |     -
|     Ticket description     |     Implement     |     Review     |
|     Estimation     |          |     Decide     |
|     Creating subtasks     |          |     Refine     |     Advice
|     Push back on complexity     |     Advice     |     Advice     |
|     Release dates     |     Refine     |     Advice     |
|     Delegating tickets     |          |     Decide     |
|     Architecture     |          |     Enforce     |     Implement
|     Review code     |          |     Review     |

### Project setup Android

1. Ask Martin to create a new repository
2. Check out the repository
3. Create a new app in the checked out repository
	- In the create-app-flow select Fragment + ViewModel (package is feature.<feature>.ui)
	- Add Kotlin support
	- Select API 21+
4. Add [Navigation Components](https://developer.android.com/topic/libraries/architecture/navigation/navigation-implementing) and convert MainActivity to use navigation to load the first Fragment
5. Add `/.idea/navEditor.xml` to _gitignore_
6. Set compile options within Android:
```groovy
compileOptions {
	sourceCompatibility JavaVersion.VERSION_1_8
	targetCompatibility JavaVersion.VERSION_1_8
}
```
7. Add data binding in app's _build.gradle_:
```groovy
dataBinding {
	enabled = true
}
```
8. Configure build types:
```groovy
buildTypes {
	stage {
		matchingFallbacks ['release']
		minifyEnabled true
		proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
	}
	release {
		minifyEnabled true
		proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
	}
}
```
9. Configure [bitrise](https://stanwood.atlassian.net/wiki/spaces/AN/pages/174293065/Bitrise+Setup+for+Android+apps)
10. Set up [Code Style](https://stanwood.atlassian.net/wiki/spaces/AN/pages/473235458/Code+Style#CodeStyle-Alternative:ktlint-gradle) -> use the alternative gradle-plugin approach further down the page (be careful with Windows, some things likely need to be adapted there)
11. Set up [architecture](https://github.com/stanwood/framework-arch-android) (general, DI etc.)

### Release Procedure Web Projects

#### 1. During Development

During the week we merge feature/bugfix branches into `develop`.

The CDCI auto-deploys to the `firebase.hosting` develop environment.

**Responsibilities**

- @devs: Codes, creates PR and merges
- @project lead: Approves PR
- @qa: Test features on `develop`.

#### 2. Creating a testable version

Thursday evening `develop` is merged into `stage` to have a stable environment for QA, test agency and client approval.

The CDCI automatically deploys to firebase.hosting stage environment.

**Responsibilities**

- @pm: Decides on time and date
- @qa: Gives the green light
- @project lead: Merges
- QA: Do smoke testing
- Test agency: Smoke testing
- Client: Smoke testing and approval

#### 3. Go Live

Between Monday and Wednesday, we deploy to production by merging `stage` into `master`.

CDCD auto-deploys to firebase.hosting production environment.

**Responsibilities**

- @pm: Decides on time and date.
- @qa: Give green light.
- @project lead: Merges.
- @qa: Do post-go-live smoke testing.

## Documentation

Always keep the project documentation in confluence up to date before moving your ticket to QA.

We document EPICs in confluence and only link from Jira tickets.

### Template

#### 1. Jira Epic

Link to Jira Epic

#### 2. User stories

List of included user stories = functional requirements (describe what’s in scope)

List of not-included user stories (describe what’s out of scope)

#### 3. Requirements

List of non-feature requirements

#### 4. Architecture
Description of API components, i.e. firebase, GAE, 3rd party APIs with links to documentation

#### 5. Views
List of all views with different platforms and screen resolutions with links to Zeplin

| Name | Link
| - | - |
| | |

#### 6. Tracking

#### 7. Links

Add link, that only links that pertain to this epic.

#### 8. Todos

Non-ticket todos

## API Guidelines

### Permissions & Setup

*IMPORTANT*: Only the backend team is allowed to create firebase, contentful or GAE instances.

They do that via a script that ensures all our policies are enforce.

If individuals set up those instances, they forget imortant security or permissions policies
and endangering our operations or even clients data.

### Background

The decision for an API can have tremendous implications for the overall effort (and pain) of developing frontend. We have seen projects exploding 10x because of APIs we had no control over.

### Preferred solution: 100% control

#### 1. Own API

When we need a complex backend, our fearless backend team build a rock solid, high performance, super scalable, 100% unit tested and monitored thing of beauty on Google App Engine in Python.

#### 2. Firebase

For smaller projects and prototypes that only need lightweight auth, data or file storage we use firebase.

**Upsides**

- Do not need a backend developer to build things
- The real-time database can function as a mock API (supports most HTTP methods including POST, PUT and DELETE)
- Changes can be monitored in real time
- Super easy integration
- Real-time database

**Downsides**

- Firebase has no build in server-side logging and makes debugging a pain
- Changing data models is virtually impossible
- Data can be broken super fast - esp. on production
- Analytics on the data structure is hard

### Third-party APIs

Should clients force us to use a 3rd party API or one they are building, we always build a proxy.

The front-end developers then draft the API as they would need it.

The proxy then translates between the frontend API and the 3rd party API.

**Upsides**

- Frontend development gets a highly optimised API
- We can monitor the 3rd party APIs for errors and report that directly to the client.
- We can hit cache and prevent outages even when the client APIs breaks.

### Monitoring

We use Stackdriver to monitor for outages, errors and speed of the API itself and Runscope as well as Postman to mimic user requests to do a full end-to-end test of the API every few minutes.

## CDCI

### Web

- Confluence (TBD)
- [Gitlab](web_gitlab.md)

### Web Gitlab

### Gitlab CDCI Web Guidelines

### Steps

#### 0. Get the project deploy from your local terminal

- Install and configure firebase CLI https://firebase.google.com/docs/cli/
- Make sure to use the correct folder for deployment, i.e. /dist /public

#### 1. Set up variables in settings/cd_ci

- FIREBASE_PROJECT_DEVELOP
- FIREBASE_PROJECT_PRODUCTION
- FIREBASE_PROJECT_STAGE
- FIREBASE_TOKEN

The central token is in 1password.

Do not protect them the variables. Otherwise, they are not be passed to the develop and stage branch.

#### 2. Set up CDCD file in root

Add a `.gitlab-ci.yml` file on `develop`, then merge down to stage and master.

Be careful! This will deploy to production.

```
image: node:latest

deploy_review:
  stage: test
  environment: develop
  only:
    - develop
  script:
    - {ADD YOUR YOUR BUILD STEPS HERE}
    - npm install -g firebase-tools
    - firebase deploy --token=$FIREBASE_TOKEN --non-interactive --project=$FIREBASE_PROJECT_DEVELOP --only hosting

deploy_stage:
  stage: deploy
  environment: stage
  only:
    - stage
  script:
    - {ADD YOUR YOUR BUILD STEPS HERE}
    - npm install -g firebase-tools
    - firebase deploy --token=$FIREBASE_TOKEN --non-interactive --project=$FIREBASE_PROJECT_STAGE --only hosting

deploy_production:
  stage: deploy
  environment: production
  only:
    - master
  script:
    - {ADD YOUR YOUR BUILD STEPS HERE}
    - npm install -g firebase-tools
    - firebase deploy --token=$FIREBASE_TOKEN --non-interactive --project=FIREBASE_PROJECT_PRODUCTION --only hosting
```

#### 3. Todos

1. Use cached folders to speed up deployment time
2. Gitlab wants testing (develop), and deployment handled differently. Research how that is done.
3. Use a centralised CDCI file to save config time.
4. Which firebase token should we use?
5. Create artefacts. Ideally, we can download a working snapshot of the build.
6. Review if all those steps are required or if we can skip some
    7. npm install -g npm@5.6.0
    8. npm install
    9. npm run build



# People operations
## Recruiting

### Guidelines

As we grow the complexity of operating the company grows. To run our company efficiently, every new team member must raise the bar.

Towards that end, every recruit must be better than the average member than that team.

Preferably every recruit is better than any member of that team.

We are acutely aware that hiring is a profoundly biased process. In addition to that, bad hires are very costly both financially and culturally.

To remedy those biases we created an elaborate process with multiple steps and at least 5 interviews.

### Process

See our private [recruitment section](https://gitlab.com/stanwood/internal/recruiting/blob/master/process.md).

This information is private, as they contain tasks candidates need to solve and questions they need to answer.

Being able to prepare in advance defeats the purpose.

## Contracts

As we have access to clients production databases, we are subject to data protection laws, non-disclosure-agreements and data-processing-agreements with all our clients.

In those contracts, we are responsible for enforcing the same clauses to all our team members.

Every freelancer and employee needs to sign 3 documents:

1. Non disclosure agreement: [Template](https://docs.google.com/document/d/18Iybj-AhccoGngPzFn8lbjRYQZOjL4uFmlt-8q-pbMw/edit?usp=sharing)
2. Data processing agreement: [Template](https://docs.google.com/document/d/1ENtLIcJlNJMY42weFzJ5KzskuGcGYVp8fSQJ5moufn0/edit?usp=sharing)
3. Core contract
    4. Freelancing contract: [Template](https://docs.google.com/document/d/1Cm8LBm9Tu9wxHfxUUhzih0u9gVu2cFFpJIqxOuPDTqc/edit?usp=sharing)
    5. Employment contract: [Template](https://docs.google.com/document/d/1MMVa-_HVo0tX1oQnxJmjHYp0oxQLP6qh4_hnz23xgt0/edit?usp=sharing)

We only use digital contracts via DocuSign. Paper is a pain.

### Freelance vs. Full time employee

We prefer working on a freelance basis because of this.

- reduces the accounting and admin overhead by 90%
- captures our entrepreneurial spirit
- is closer to our processes

Some states have "fake-freelancer-laws" that prohibits this practice sometimes. In those cases, we go with the employment contract.

## Compensation

### Background

We provide a transparent and unified payment scheme based on hourly rates that reflect the impact for the company, responsibilities and scarcity of the skill set.

### Developers and project managers

- New guys: 25€
- Mid-level: 30€
- Senior: 35€
- Project lead: 40€
- Team lead: 45€

### *QA manager*

- Junior: 15€
- Mid: 20€
- Senior: 25€
- Team lead: 30€

We *do not downgrade* existing hourly rates. We will heal the existing "injustices" with a general rate increase in the next years.

### Full-time employees

Full-time employees get a wage based on the gross cost hourly rate + 20% (employer payment for pension health insurance, ...) x 8 h / day x 220 d / year.

So if an employee and a freelancer work that 1760 h per year, they cost the company the same amount.

220 d is the average per full-time employee in Germany.

1700 h was the average of full-time freelancer at stanwood last year.

**Example**


*Employee:*

Wage: 40 €/h x 8 h/d x 220 d/y x 80% = 56,320 €

Benefits: 40 €/h x 8 h/d x 220 d/y x 20% = 14,080 €

*Freelancer*

Fee: 40 €/h x 8 h/d x 220 d/y = 70,400 €

## Onboarding

New employees need these accounts.

All accounts (except GitHub and Gitlab) must be the @stanwood.de email.

### 0. TODO new employee
Ask them to read this handbook thoroughly!

### 1. E-mail
https://admin.google.com/stanwood.de/AdminHome

Lead sends a password to private email of the user and asks new employee to change PW right after logging in.

All following invites (if not mentioned otherwise) go to that new Email address.

### 2. Slack
https://stanwood.slack.com/admin

1. Add user
2. Add user to group

The new handle will only be usable after registration.

### 3. Github and Gitlab
Add private GitHub handle to teams

https://github.com/orgs/stanwood/teams

### 4. Toggl

https://www.toggl.com/app/team/1229046

1. Add user
2. Add user to a group (used in accounting)

### 5. Jira

https://stanwood.atlassian.net/admin/users/add

Username: Only the first name

Add to the group: "Developers."

### 6. 1password

https://stanwood.1password.com

### 7. Zeplin

https://app.zeplin.io/organization/dashboard.html#oid=58a2f20e6d53d90f4bfd9fee

### 8. Holiday Calendar

https://calendar.google.com/calendar/b/1/render#

### 9. Applause

https://platform.applause.com/account/usermgmt/teammgmt

### 10. Add birthday and anniversary to Hannes Calendar and Spreadsheet

https://docs.google.com/spreadsheets/d/1Te6wxnS8Bu_WSHf4Eio567Uk4_kmA8hduZL9HUK6ksc/edit#gid=0

### 11. MyTaxi

https://de.mytaxi.com/login.html?target=corporate

### 12. Trello

https://trello.com/stanwood3/members

### 13. Zohoo (Employees only)

https://expense.zoho.com/app#/usersmanagement/users

### 14. TODO for new employee: Setup Jira/gitlab + Toggle Sync:
Install and configure our own Toggl Chrome extension fork: https://gitlab.com/stanwood/internal/tools/toggl-sync/wikis/home

New devs: Enable Toggl + Jira Sync, please: https://toggl-jira-sync.appspot.com/login (still needed with the aforementioned?)

### 15. After they accepted everything:

Add them to the project and general slack channels.
Welcome them in #general

## Continuous Performance Management

### 1 on 1 reviews
We do continuous performance management. Leads have regular 1 on 1 reviews with their team members.
We centre on 5 questions:
- What are you working on?
- How are you doing; how are your OKRs/Goals coming along?
- Is there anything impeding your work?
- What do you need from me to be (more) successful?
- How do you need to grow to achieve your career goals?

This is the team members meeting. Tone and agenda should be set by the team member. The lead is there to coach and learn.

Background information:
https://gitlab.com/stanwood/blog/blob/master/inspirations_from_books.md#L208-214

## People development

### Background

stanwood is the embodiment of change. We want to improve every day. One crucial step (others are Analyses, Planning, Doing and Collecting Data) is the Review.

### 0. Performance review

Every team member has a performance review meeting with their team lead every quarter.

The Q3 review is the Annual Review where we also add 360° feedback.

Apart from general feedback, your lead discusses these points:

- Developer ideals
- Team KPIs and how you might contribute
- 360° feedback

With the Q1 feedback in 2019, we add personal goals and personal KPIs.

### 1. Goals

Numerous studies have shown, that setting goals increase a teams performance and personal happiness.

We already have our company goals for each year.

We start developing (bottom-up) goals for every team in 2018 and every person in 2019.

Those goals can - but must not overlap - with the higher team and company goals.

Goal achievement is purely developmental and is not tied to your compensation or career.

### 2. KPI

We are blessed with numerous sources of data that can help us understand our performance as a group.

When setting goals, we try to use data to inform our decision making.

We also use data to identify patterns and improve our operations.

We never use data to control or punish people.

KPIs are purely developmental and are tied to your compensation or career.

#### Developer KPI

1. Estimation accuracy
2. Feature implementation speed
3. Bug fixing speed
4. % worked on bugs vs features
5. % tickets passing QA on the first try

#### QA KPI

1. % tickets passing QA on the first
2. Total number of tickets reviewed
3. Number of bugs found by the client

#### Project management KPI

1. Estimation accuracy
2. % worked on bugs vs features
3. % tickets passing QA on the first try
4. Project profitability

### 3. 360° Feedback

We want to give you a fuller picture of how you are perceived by our fellow Woodies and even clients (for project managers).

For that end, we identified up to 7 people each Woody worked with most - that gives anonymised feedback and vice versa.

This survey is purely developmental. Nothing you say here impacts compensation or the career of that person or yours.

#### I. HOW TO

Please choose your feedback partners from this list
Pairings

moreover, leave them short and actionable feedback here

Form

#### II. EXAMPLES
Keep
"Fun to work with, a good mentor, always available to help when I have technical issues, or I am blocked with a task."

Change
"Share more from his/her experience in programming channels so I can learn, delegate more interesting tasks where I can improve my skills, "

#### III. NOTES
1. This survey is anonymous towards the person you give feedback.
2. Hannes sees all answers and your name.
3. Team leads see all answers for their team and your name.
4. The team lead synthesises all feedback to ensure anonymity.
5. If you do not have 7 people on your list, click "NEXT" and leave those sections empty.

### 4. Leadership feedback

Our team leads get a bit more detailed feedback.
Every team member answers the following 12 questions.

- My team lead gives me actionable feedback that helps me improve my performance.
- My team lead does not "micromanage".
- My team lead shows consideration for me as a person.
- My team lead values the perspective I bring to the team, even if it is different from his own.
- My team lead keeps the team focused on our priority results/deliverables.
- My team lead regularly shares relevant information from his bosses.
- My team lead has had a meaningful discussion with me about career development in the past six months.
- My team lead communicates clear goals for our team.
- My team lead has the technical expertise required to effectively manage me.
- I am satisfied with My team lead’s overall performance as a manager.
- What should My team lead keep doing?
- What should My team lead change?

This is based on Google's research what kind of leader makes teams most effective.

https://qz.com/1058760/manager-feedback-at-google-employees-are-asked-13-questions-about-their-bosses-as-part-of-a-semi-annual-review-goog-googl/

## Training

### Background

We want to get better at what we do every day. For that we offer the following tools:

### Courses

We pay both hours and courses for everyone to learn any (really any) of the trades that we use at stanwood. If you are a coder, who wants to learn to design: You are welcome — a QA manager who wants to learn Accounting: Yes, please.

Just ping the MD or your team lead, and we set you up.

### Masterclasses

Every Wednesday at 10 am CET we hold a masterclass where one of our own gives a one hour talk (strictly enforced) about an exciting topic.

The head of engineering will select the mandatory audience on Monday, but everyone including clients is invited to join.

We use hangouts and youtube live streaming for that.

Check #masterclass for more info.

### Book club

Todo: Create a Stackoverflow-like voting system for books and summaries and discussions.


## Meet-ups

#### Retreats

We meet 2x per year for a two-day workshop.

#### Workshops

Between the retreats, teams can meet as often as they see fit.

#### Grill the boss

Every week the MD invites one happy Woody to fly to Munich and spent an evening cooking and drinking.
I did that with the latest batch of Wookies and selected Woodies over the last weeks, and I loved it.

All expenses paid of course.

As soon as we hit 52 team members, I will up this to 2x week.

#### Meet a Woody

If you want to meet a Woody in person, we are sponsoring the flight with up to 200€ and a stay at the hotel up to 100€.

Sent me a direct message to the MD and explain, why you want to go.

We will be generous.

Can be project related or just because you always wanted to meet the other person.

Groups are also allowed, for instance, to do a team kick off or celebratory launch party.

## Travel and Expensing

### Background

We try to minimize the accounting overhead of traveling as much as possible. 

Because invoices for flights are issued to the travel - not the company - this creates an accounting nightmare.

The bank or credit card statement just says "123.24€ Lufthansa". Then the hunt for the receipts beginns.

So we turn this around. The person who get's the invoice pays themselve and then expenses.

### Booking process

All travels are organized in Trello.

1. When we need a travel, we create a card in "Planing". 
2. We add location, attendees, dates and required actions in a checklist and assign who is responsible for it.
3. As soon as everything is booked, Office manager moves the card to "Booked"
4. After the event, the main attendee writes a quick summary (optional) and moves to "Done"

Then add your flight times to the holiday calendar with "{my name} {flight number}", in order for the organizers see your flight times.

### Hotels

Our office manager books hotels in #office centrally for everyone. Invoices are paid by the company via credit card.

### Flights

Every team member books and pays their flights, train rides or car trips themselves. 

### Dinners

When two Woodies meet or meet with a client, the company pays for food.

### What we pay for

As a general rule: You are a guardian of the company. We trust you to make the right choices and spent money wisely.

**Flights**

- Economy class on first tier airlines (Lufthansa, BA, El-Al)
- Direct flights outside business hours
- Upgrade your seats to emergency exit row, if you are planing to work
- Frequent travelers may book business class

**Trains**

- Business or first class if you wanna work
- Economy class if you wanna chill

**Hotels**

- Motel One
- London: Pullmann
- Frequent travelers can book 25hours

**Conference Rooms**

- Wework, Business Link or Mindshare

**Food during events**

- Breakfast at the hotel
- Lunch paid by the company
- Dinner paid by the company
- Drinks paid by the company, if MD is present

### Expensing

#### Freelancers

1. Expense your travels via *seperate* invoices right after the event. 
2. Add the original receipts to the invoice (same PDF).
3. Sent them to invoice@stanwood.de
4. The MD wires the money every Monday.

The seperate invoice if for accounting purposes. We book those invoices unto a different account.

#### Employees

Use the zohoo.com

1. Upload your receipts (only the ones **you** paid) and car travels
2. Edit VAT, project, category and amounts
3. Submit receipts for approval
4. The MD approves all travel expenses every Monday
5. Employees gets the "Reimbursement Reports" via email
6. Download the report and sent it to `invoice@stanwood.de`
7. The MD wires the amounts every Monday.

Step 6 is nessassary to for accounting. We book your travel on the cost center and cost type of your team. 
The auto assigning is done via your email address.

## Time Tracking

### Background

We (mostly) invoice our clients based on hours worked.

We get paid by the hour. Our clients honour us with a tremendous amount of trust - in return we give them total transparency.

They expect splits by ticket or overhead tasks.

### Working hours

Entirely up to you, but make it transparent to the project team, and the team leads.

### Public holidays and vacations

Please use the GCal Holiday calendar. A 4-week head start would be good, so the leads can plan the projects.

Check with the PM that we have no releases or major events planned during that time.

Make sure to answer all messages in slack at least within 1h during 10:00 and 18:00 CET. This is only required if you have not marked the day or period in the Holiday calendar.

Post in #status, should you be away from the keyboard for more than 1 h during 10:00 and 18:00 CET

### Workshops

When participating in or preparing for a workshop book on "Internal  Workshops".

### Travel

We do not pay for the time you spent travelling, so please do not book it.

However, if you bring your laptop and work while on transit, please usually book on the project, and you will, of course, be paid.

### Masterclasses

Please book the time for participating and preparing for Masterclasses on "Internal Workshops".

### Sales

When you are helping with a pitch or a sales presentation, please book on project "Sales".

### Naming

Use the Jira ticket ID and description, i.e. LEN-01 My awesome new feature
Assign a project.

Best do it realtime. Use Toggl-Jira chrome extension

Book non-ticket project work on one of the Tasks in Toggl:

- Client communication
- Internal communication
- Estimation
- Ticket review
- Test agency
- QA
- Testing
- Deploying

On invoices they get mapped to

1. Added pro rata to tickets
    - Internal communication
    - QA
    - Testing

2. Consulting
    - Client communication

3. Concepting
    - Estimation

4. Project management    
    - Ticket review

5. Review test agency
    - Test agency

6. Deployment
    - Deploying
    - Service Desk

Book non-project work on project Travel & Admin. Try to book as much project hours as possible.

## Offboarding

We strive to be a company that does not have to fire people.

However, even the best hiring and people development process fails from time to time, and we have to part ways with a person.

We give feedback at least every 3 months during our performance reviews.

There are 5 reasons for ending or not extending a contract:

1. Gross negligence or malicious behaviour hurting a client or the company
2. Hurting another Woody or poisoning a team
3. Not following our Vision and Ideals
4. Financial strains the company cannot overcome
5. The employee asks us to end the contract

### Process

- The team lead and the MD decide to start the process.
- All team leads to discuss the issue.
- Team leads have a veto that the MD can override.
- The team lead of the team member and the MD decide to end the contract.
- The employee is told by his lead with Hannes present
- That he/she is officially on-notice for four weeks
- What changes are required to stay in the company
- Daily check-ins with Hannes and the respective Lead 
- After 4 weeks the leads and Hannes review the process and decide to either
- End the contract
- Extend the contract
- Extend the on-notice period by another 4 weeks
- The employee is told the decision by the Lead with Hannes present
- Hannes will inform the company about the decision and the reasons
- Should the contract be ended, we offer a 4 week grace period during which
- The employee gets full pay
- stanwood uses all their contacts to find another engagement for the developer
- The developer must show 200% commitment during those 4 weeks including reporting every ticket when they start and end working on to the team lead and project manager

This process is only valid for reasons 3. If we ever need to lay off people due to financial strains, we honour the contracts.

Before it comes to that we

- Try to increase sales.
- Try to get funding.
- Try to find developers who can work part-time, take sabbaticals or work somewhere else for a bit.

### Last day

#### Ops

- Disable all accounts from onboarding

#### MD

- Write a personal note to the person leaving
- Thank the person on #general

#### Person leaving

- Reply to MD post with a personal message to the team and leave your private email to stay in touch [Optional]
- Remove stanwood GDrive
- Remove all accounts and passwords from password manager
- Remove all code repository and stanwood docs from your computer
- Remove all private data from your computer [if company computer]
- Reset all devices to factory settings [If company hardware]
- Sent hardware back to MD [if company hardware]
- Sent last invoice (if freelancer)

## Alumni

Every team member who leaves the company on his accord will become part of the esteemed club of stanwood alumni.

They keep their email and slack account and get invited to all company retreats and workshops.

Travel expenses are paid but not the hours.


# Support processes
## Accounting

### Tools & Processes

All ingoing an outgoing invoices are digitised and then sent to invoice@stanwood.de (alias of info@stanwood.de).

That forwards it to candis.io for processing.

Payments are made every Monday morning from candis.io and the local tool MoneyMoney by the MD.

On the next Monday that is not the 1st of the month is closed.

The MD adds cost centres and accounts numbers to all invoices.

Our accountants (Goldstein Consulting) get an export of all data, do some accounting magic and create cost centre reports for all teams.

### Tags

Every invoice gets 3 tags.

1. Type: i.e. Travel (0..9,999)
2. Company: i.e. Uber (10,000..99,999)
3. Cost center: i.e. Team android (1000..999,999)

### Cost centres

Every lead is responsible for a cost centre.

- 1000 General Management @hannes
    - ~~1100 Sales & Marketing~~
        - 1110 Sales @richard
    - ~~1200 Finance~~
- ~~2000 Product & Project Management~~
    - 2100 Project Management @richard
    - 2200 Quality Assurance @romy
- ~~3000 Software Development~~
    - ~~3100 Mobile Development~~
        - 3110 iOS Development @tal
        - 3120 android Development @sven
    - ~~3200 Web Development~~
        - 3300 Backend Development @marcin
        - 3400 Web Frontend Development @arni
- ~~4000 Operations~~
    - 4100 Hosting @marcin
    - 4200 Tools @hannes

## Freelancer Invoices

    Invoice needs to be in German or English. Can have your language as well.

    ### Required elements

    - Full name of the company or freelancer
    - Full address of the company or freelancer
    - Full address of stanwood
    - Tax ID or VAT ID even if there is no tax applicable
    - Date of the invoice
    - The month of service provided (usually the prior month
    - Invoice number: Incremental
    - Hours worked and hourly rate
    - Description i.e. "Software development", "consulting"
    - Add extra lines for travel expenses
    - Net sum
    - VAT tax rate and sum (usually 0% and 0€ because of reverse charge)
    - Gross sum (net + tax)
    - If reverse charge
        - add "No tax due to reverse-charge."
        - add "stanwood VAT ID: DE 263524087".
    - IBAN and BIC

    In Germany, you might be a "Kleinunternehmer". Check with your accountant and then add this to your invoice:

         "Als Kleinunternehmer im Sinne von § 19 Abs. 1 UStG wird keine Umsatzsteuer berechnet."

    ### Process

    1. On the first of the month
    2. Grab your hours from [Toggl](https://www.toggl.com/app/reports/summary/1229046/period/prevMonth)
    3. Create an invoice with those hours
    4. Sent it to invoice@stanwood.de and put the MD on CC.
    6. After 2-3 weeks you should have the money on your account.


## Processing invoices

We are using a central email 'invoice@stanwood.de' to get all invoices into our systems and incorporates'.

Those emails get forwarded to a candis.io account (to have a central log for all emails) and then forwarded to corporates invoice address.

Corporate processes all those emails and assigns them to our office assistants. They assign a cost centre and approve it.

After 1-2 days, those invoices are assigned to our MD who approves as well.

After that, another corporate employee approves the invoice as well. With that, the invoices are paid 1-5 days later.

The whole process takes 10 business days at best and sometimes up to three weeks.

## Sales and Marketing

### Our skills

#### Digital Transformation

- App data
- Consulting: Big data scientiesty + business casper
- Business planing and concepting
- Street credibility 
- ON AIR
- Corporate background
- Funke Exit

#### Product management
- Lean development: Scope Management
- Designen, Coden, Deployment, Hosting, Maintaining, Marketing, Monetization

#### Distributed
- Best and happiest coders
- Around the clock
- Mindset
- Modern
- One person in every country #glocal
- "Cultured code"
- Fits to distributed clients
- Digital nomad family
- Transparency

### Passions

- Fun with nerds
- Techies
- Number chruncher
- Food & Booze
- Work 2.0
- First 100% distributed agency
- Distributed
- Lean product development
- Digital transformation
- Build stuff

### Tilt

- Overlap of our 3 core skills.
- Define "Stanwood Persona" after client personas

### Segments

1. "Interims CTO" Inkubator
2. "Workbench" for agencies
3. "Industry 4.0"
- SME
- Agencies

### Channels

- Website
- Landing page
- Blogs
- Youtube
- Pintrest
- Facebook
- Instagram
- Linkedin/xing
- Twitter
- Podcasts
- Conferences
- Own events/workshops
- Magazine
- Webinars
- Masterclasses/webinars
- Ads
- Newsletter
- Case studies
- White paper
- Interviews
- Influencer
- Book
- Merchandise
- PR
- Direct mail
- Cold calls

### Content

#### 1. Distributed

- Meta: Work 4.0, Benefits, Lean
- https://gitlab.com/stanwood/handbook/
- Add new processes
- Focus on core processes
- Culture
- Health
- Home stories & portraits
- Origin story
- Sales Strategy
- Workation
- Future of stanwood (3 / 5 remote)
- Hannes/Richard/Martin private digital dungeon

#### 2. Product management

- MVP - Why
- Global development process - How to get an app to the App store in 2 months
- Data driven product development
- Feature creep
- A/B
- big data
- machine learning
- Agile software development
- QA (add applause)
- Monetisation strategies
- News apps
- (Jean)
- App Store Optimization
- Product stories
- All our products & interviews
- Hosting & API
- No admins
- No server
- Highly scalable
- Monitoring
- Google AI APIs
- Client communication
- Bots
- Lesson learned
- Analytics
- Onboarding
- useronboard.com
- IAP & Paywalls
- Best practises
- Why Apple will reject you
- Apple approval strategy
- 1.0 in Luxembourg
- Server side feature flags
- Design processes (re Wunderdeals)
- Retention and conversion optimisation (CRM)
- Push notifications & deep links
- CRM
- Cross platform coding (Flutter, Phone Gap, Xamarin, react)
- Sync iOS and android development
- Multi project management (defrag)
- How get 5 star ratings
- App development costs
- Multi-tenant apps
- Next tech: Bots, AI...

#### 3. Digital Transformation

- Product stories
- Change management
- Internal
- Clients
- Tools: Introduction
- Content Marketing
- How to make your company future proof?
- No back-office (candies, Zohoo, Goldstein, weworks/mindspace)
- Paperless

### Marketing Operations

#### 1. Social Media

We schedule post to instagram, facebook, twitter (MD and company accounts), 
linkedin (MD and company accounts) and xing (via API) via [Hubspot's social 
media publishing tool](https://app.hubspot.com/social/4628060)
 at least 7 days in advance. 

We write and plan the posts in a [GDoc](https://docs.google.com/document/d/1E4mt5aAR0tQEbOnuPBJ7nzczP2morFc9oUH18Fd3LRs/edit#).

**Goal**

Publish one post on every channel every day.

#### 2. Blog

We plan our blog articles in a [GSheet](https://docs.google.com/spreadsheets/d/1nMkUHs_I-A0i-QDiyXCdA_UaDBXNLZqX0gg_TrbjQX4/edit#gid=0)